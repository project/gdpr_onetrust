<?php

/**
 * @file
 * This file provides function to store common JS across all websites.
 */

/**
 * Create an array to update the common JS files.
 */
function cookie_js_path() {

  return $cookie_js_path = [
    ONETRUST_COOKIE_BLOCKING_PERFORMANCE => [],
    ONETRUST_COOKIE_BLOCKING_FUNCTIONAL => [],
    ONETRUST_COOKIE_BLOCKING_TARGETTING => [],
    ONETRUST_COOKIE_BLOCKING_MEDIA => [],
  ];

}
